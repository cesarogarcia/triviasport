

/******************************MOSTRAR----PANTALLA---OCULTAR********************************* */
let section_inicio = document.getElementById("inicio");
let section_preguntas = document.getElementById("preguntas");
let section_score = document.getElementById("score");
let butonFutbal = document.getElementById("botones_temas_futbol");
let butonBk = document.getElementById("botones_temas_basket");
let butonBx = document.getElementById("botones_temas_boxeo");
/****************************************POP-UP***************************************************/
let section_pop_ok = document.getElementById("pop-up-ok")
let overlay = document.getElementById('overlay')
let section_pop_basura = document.getElementById("pop-up-cagada")
/*********************************VARIABLES ARRAY PREGUNTAS Y RESPYESESTAS************************* */

let preguntasfb = [
  {
    pregunta: "1.¿Qué clubes crees que han jugado todas las ediciones de La Liga?",
    respuesta: "FC Barcelona, Real Madrid, Athletic de Bilbao",
    opciones: ["FC Barcelona, Real Madrid, Athletic de Bilbao", "Real Madrid y Atlético de Madrid", "Athletic de Bilbao y Real Madrid", "FC Barcelona y Real Madrid"]

  },

  {
    pregunta: "2.¿Cuántas copas de la Liga tiene el Sevilla FC?",
    respuesta: "Una",
    opciones: ["Cero", "Una", "Dos", "Tres"]

  },

  {
    pregunta: "3.¿Cuál es el futbolista que más goles ha marcado en la historia de la Liga desde 1928?",
    respuesta: "Messi",
    opciones: ["Raúl", "Zarra", "Messi", "Cristiano Ronaldo"],

  },

  {
    pregunta: "4.La temporada más goleadora de la historia de la Liga fue la de 1996-97. ¿Cuántos goles se marcaron?",
    respuesta: "1271",
    opciones: ["756", "1271", "1045", "934"],

  },

  {
    pregunta: "5.¿Cómo se le conoce coloquialmente a la selección Venezolana de Fútbol?",
    respuesta: "La Vinotinto",
    opciones: ["Los noveleros", "Los Tricolores", "La Vinotinto", "El grupo de los Chamos"],

  }
]
/**************************ARRAY bx*********************************************************/
let preguntasbx = [
  {
    pregunta: "1.En 1936, en la Arena Nacional ocurrió el primer caso de un boxeador mexicano muerto en el ring; se trató de:“Joe” Conde.",
    respuesta: "Francisco “Paco” Sotelo",
    opciones: ["Joe Conde", "Francisco “Paco” Sotelo", "Rudy Coronado", "Julio Cesar Chavez"]
  },

  {
    pregunta: "2.Primer boxeador mexicano que cobró un millón de dólares por una pelea.",
    respuesta: "José Pipino Cuevas",
    opciones: ["José Pipino Cuevas" ,  "Jorge “El Maromero” Páez" , "Salvador Sánchez" , "Jorge Maravilla Martinez"]
  },

  {
    pregunta: "3.Barrio de La Habana, famoso en los años 40 por la gran cantidad de boxeadores que producía.",
    respuesta: "San Antonio",
    opciones: ["San Antonio", "San Nicolás", "Negro", "Quintero"]
  },

  {
    pregunta: "4.Fue candidato a diputado por el PST en 1982 y también actor de cine y de teatro.",
    respuesta: "Rubén “Púas” Olivares",
    opciones: ["Rubén “Púas” Olivares", "Raúl “Ratón” Macías", "Luis “Kid Azteca” Villanueva", "El chulo Papi"]
  },

  {
    pregunta: "5.Cantina donde se reúnen semanalmente numerosos ex boxeadores para recordar viejas glorias.",
    respuesta: "La hija de los Apaches",
    opciones: ["La Guadalupana", "Moctezuma", "La hija de los Apaches", "Mi Chelo"]

  }

]
/**************************BASKET_ARRAY*********************************************************/

let preguntasbk = [

  {
    pregunta: "1.¿Cómo le fue a España en su primer Eurobasket (1935)?",
    respuesta: "Plata",
    opciones: ["Plata", "Oro", "Bronce", "Quinta"]
  },

  {
    pregunta: "2.¿Cuál ha sido el peor resultado de nuestra selección en este torneo?",
    respuesta: "Decimoquinto puesto Turquia 1959",
    opciones: ["Decimoquinto puesto Turquia 1959", "Decimo tercero yugoslavia 1961", "Undecimo URSS 1965", "Finlandia 1967"]
  },

  {
    pregunta: "¿3.¿En qué Eurobasket se ganó por primera vez a la Unión Soviética para acabar logrando una plata?",
    respuesta: "Barcelona 1973",
    opciones: ["Barcelona 1973", "Alemania 1971", "Francia 1983", "Yugoslavia 1975"]
  },

  {
    pregunta: "4.¿Cuál de estos seleccionadores españoles tiene el récord de participaciones en el torneo con 13??",
    respuesta: "Antonio Diaz Miguel",
    opciones: ["Antonio Diaz Miguel", "Lolo Sainz", "Sergio Scariolo", "Mariano Manent"]
  },

  {
    pregunta: "5.¿Quién anotó la canasta decisiva contra la URSS para asegurar la plata de 1983",
    respuesta: "Epi",
    opciones: ["Epi", "Fernando Martín", "Andrés Jiménez", "Corbalán"]
  }
]
/*********************************mostrar pantallas futbol**************************/
section_preguntas.style.display = "none";
section_score.style.display = "none";
let array = [];

function mostrarPantallaFutbal() {
  array=preguntasfb;
  section_inicio.style.display = "none";
  section_preguntas.style.display = "block";
  section_score.style.display = "none";
  mostrarpreguntasfb();
  mostrarrespuestasfb();
 
}

butonFutbal.addEventListener("click", mostrarPantallaFutbal);

/*********************************mostrar pantallas basket**************************/

function mostrarPantallaBk() {
  array=preguntasbk;
  section_inicio.style.display = "none";
  section_preguntas.style.display = "block";
  section_score.style.display = "none";
  mostrarpreguntasfb();
  mostrarrespuestasfb();
 
}

butonBk.addEventListener("click", mostrarPantallaBk)

/*********************************mostrar pantallas BX**************************/

function mostrarPantallaBx() {
  array=preguntasbx;
  section_inicio.style.display = "none";
  section_preguntas.style.display = "block";
  section_score.style.display = "none";
  mostrarpreguntasfb();
  mostrarrespuestasfb();

}

butonBx.addEventListener("click", mostrarPantallaBx)

/*********************************FUNCIONES mostrar preguntas y respuestas **************************/
let currentcuestionfb = 0
let contarPregunta= 0

function mostrarpreguntasfb() {
  let preguntafb = document.getElementById("cuestion");
  preguntafb.innerHTML = array[currentcuestionfb].pregunta; 
}

function mostrarrespuestasfb() {
  let opcionesfb = document.getElementById("contenedorespuestas");
 
  opcionesfb.innerHTML ="";
  
  for (let i = 0; i < array[currentcuestionfb].opciones.length; i++) {  
    let botonradios = document.createElement('input');
    let actual = document.getElementById("actual");
    
    botonradios.setAttribute('type', 'radio');
    botonradios.setAttribute('name', 'respuestaRadio');
    botonradios.className = "opcionesbotonradios";
    botonradios.setAttribute('value', array[currentcuestionfb].opciones[i]);
    let textoradios = document.createElement('span');
    textoradios.innerHTML=array[currentcuestionfb].opciones[i];
    actual.innerHTML = "Has Respondido:  " +currentcuestionfb + "/5";

   

    opcionesfb.append(botonradios);
    opcionesfb.append(textoradios);
   
      }DimequePreguntaToy();
}

function DimequePreguntaToy(){
  let preguntaActual = document.getElementById("toyPregunta");
 
  contarPregunta++
  preguntaActual.innerHTML=  "Tu Pregunta Actual:  " +contarPregunta +"/" + array.length ;
    
  }


/*********************************Boton de validar**************************/
let puntuacion = 0;
let botonvalidar = document.getElementById("boton_preguntas_validar");

function haPulsadoOpc(){
  let  opcionesradionull = document.querySelector("input[name='respuestaRadio']:checked");
  if  (opcionesradionull===null){
    return false
  } else {
    return true
  }
}

function tomarElcheck(){
   let elegido = document.querySelector("input[name='respuestaRadio']:checked").value;
   return elegido;
}


botonvalidar.addEventListener("click", validarElcheck);
function validarElcheck() {
  let chequekok = array[currentcuestionfb].respuesta;
  let opcionMarcada=haPulsadoOpc();
  
  if (opcionMarcada===true){
    let opcionUsuario=tomarElcheck()
    
    if  (chequekok===opcionUsuario){
      puntuacion++;
      alert("EXCELENTE");}
      else {alert("Uy Que Mal Rollo")}
      
      if (currentcuestionfb==4){
        mostrarscorefinal()}
        else{pasarPregunta();}
      
      }
      else{alert("Por Favor Elija Una Opción")}
      return puntuacion;}

function pasarPregunta() {
  currentcuestionfb++;
  mostrarpreguntasfb();
  mostrarrespuestasfb();}
  
function mostrarscorefinal() {
  section_inicio.style.display = "none";
  section_preguntas.style.display = "none";
  section_score.style.display = "block";
  mostrarpuntos();
  aprobadoAplazado();}

function mostrarpuntos() {
  let puntitos = document.getElementById("puntototales");
  puntitos.innerHTML ="Tu Puntuación es:  " + puntuacion;}

function aprobadoAplazado() {
  let pasaono = document.getElementById("aprobadoAplazado");
  if (puntuacion  >= 3){
    pasaono.innerHTML = "Has Aprobado";}
    else {pasaono.innerHTML = "Has Reprobado";}}


/**********************Boton Exit*****************/
document.getElementById("boton_salir").addEventListener("click", function(Event) {
  section_inicio.style.display = "block";
  section_preguntas.style.display = "none";
  section_score.style.display = "none";
  resetear(); });

document.getElementById("boton_preguntas_validar").addEventListener("click", function(Event) {
  });

function resetear(){
  currentcuestionfb=0;
  puntuacion=0;
  contarPregunta=0}

document.getElementById("boton_salir2").addEventListener("click", function(Event) {
  section_inicio.style.display = "block";
  section_preguntas.style.display = "none";
  section_score.style.display = "none";
  resetear();});

/********************** final Boton Exit*****************/
/****************************************************FUNCION MOSTRAR POP-UP_malo********************************/
/*document.getElementById("skip")
.addEventListener("click" , function (event){
  section_pop_ok.style.display = "none";
  section_preguntas.style.display = "block";

});
document.getElementById("skip2")
.addEventListener("click" , function (event){
  section_pop_basura.style.display = "none";
  section_preguntas.style.display = "block";

});*/

/************************************************FUNCION_LLAMAR_BOTON_SPIP***********************************/
/*
function mostrarPop_up_bueno() {
  section_preguntas.style.display = "none";
  section_pop_ok.style.display = "block";

}
function ocultar_pop_up() {
  section_preguntas.style.display = "block";
  section_pop_ok.style.display = "none";
}


function mostrarPop_up_malo() {
  section_preguntas.style.display = "none";
  section_pop_basura.style.display = "block";
}
function ocultar_pop_up_malo() {

  section_preguntas.style.display = "block";
  section_pop_basura.style.display = "none";
}
*/

/********************** Cesar-Unai-Jose*****************/